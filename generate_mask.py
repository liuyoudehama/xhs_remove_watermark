import cv2
import sys


def main():
	path = sys.argv[1]
	#image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
	image = cv2.imread(path)
	
	height, width, channels = image.shape

	threshold = 128

	for row in range(0, height):
		for col in range(0, width):
			for chan in range(0, channels):
				if image[row][col][chan] > threshold:
					image[row][col][chan] = 255
				else:
					image[row][col][chan] = 0

	cv2.imwrite('output.jpeg', image)


def draft():
	# Python program to explain cv2.putText() method
	# importing cv2
	# path
	path = sys.argv[1]

	print(path)

	# Reading an image in default mode
	image = cv2.imread(path)
	

	# Window name in which image is displayed
	window_name = 'Image'
	# text
	text = '1018146450'
	# font
	#font = cv2.FONT_HERSHEY_SIMPLEX
	font = cv2.FONT_HERSHEY_PLAIN
	

	# org
	org = (1040, 1654)

	# fontScale
	fontScale = 2.4

	# Red color in BGR
	color = (0, 0, 255)

	# Line thickness of 2 px
	thickness = 2

	# Using cv2.putText() method
	image = cv2.putText(image, text, org, font, fontScale,
					color, thickness, cv2.LINE_AA, False)

	# Using cv2.putText() method
	#image = cv2.putText(image, text, org, font, fontScale,
	#				color, thickness, cv2.LINE_AA, True)

	# Displaying the image
	cv2.imwrite('output.jpeg', image)

	#cv2.imshow(window_name, image)



if __name__ == '__main__':
	main()